﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FeetToMeters
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            //Declare doubles needed for calculation
            double feet, meters;

            //Assign and calculate meters

            feet = double.Parse(feetInputTextBox.Text);

            meters = feet * 0.3048;

            //Assign meters to output

            meterOutputLabel.Text = meters.ToString();
        }

        private void ClearFormButton_Click(object sender, EventArgs e)
        {
            //Clear all of the boxes on form
            meterOutputLabel.Text = " ";
            feetInputTextBox.Text = " ";
        }
    }
}
