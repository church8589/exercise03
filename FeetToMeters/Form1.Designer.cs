﻿namespace FeetToMeters
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.feetLabel = new System.Windows.Forms.Label();
            this.feetInputTextBox = new System.Windows.Forms.TextBox();
            this.feetToMetersLabel = new System.Windows.Forms.Label();
            this.meterOutputLabel = new System.Windows.Forms.Label();
            this.convertButton = new System.Windows.Forms.Button();
            this.clearFormButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // feetLabel
            // 
            this.feetLabel.AutoSize = true;
            this.feetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.feetLabel.Location = new System.Drawing.Point(40, 59);
            this.feetLabel.Name = "feetLabel";
            this.feetLabel.Size = new System.Drawing.Size(213, 20);
            this.feetLabel.TabIndex = 0;
            this.feetLabel.Text = "How many feet to convert";
            // 
            // feetInputTextBox
            // 
            this.feetInputTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.feetInputTextBox.Location = new System.Drawing.Point(260, 58);
            this.feetInputTextBox.Name = "feetInputTextBox";
            this.feetInputTextBox.Size = new System.Drawing.Size(100, 26);
            this.feetInputTextBox.TabIndex = 1;
            // 
            // feetToMetersLabel
            // 
            this.feetToMetersLabel.AutoSize = true;
            this.feetToMetersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.feetToMetersLabel.Location = new System.Drawing.Point(105, 177);
            this.feetToMetersLabel.Name = "feetToMetersLabel";
            this.feetToMetersLabel.Size = new System.Drawing.Size(197, 20);
            this.feetToMetersLabel.TabIndex = 2;
            this.feetToMetersLabel.Text = "Feet entered in Meters:";
            // 
            // meterOutputLabel
            // 
            this.meterOutputLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.meterOutputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meterOutputLabel.Location = new System.Drawing.Point(109, 220);
            this.meterOutputLabel.Name = "meterOutputLabel";
            this.meterOutputLabel.Size = new System.Drawing.Size(193, 23);
            this.meterOutputLabel.TabIndex = 3;
            // 
            // convertButton
            // 
            this.convertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertButton.Location = new System.Drawing.Point(167, 261);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(75, 29);
            this.convertButton.TabIndex = 4;
            this.convertButton.Text = "Convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // clearFormButton
            // 
            this.clearFormButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearFormButton.Location = new System.Drawing.Point(148, 308);
            this.clearFormButton.Name = "clearFormButton";
            this.clearFormButton.Size = new System.Drawing.Size(116, 28);
            this.clearFormButton.TabIndex = 5;
            this.clearFormButton.Text = "Clear Form";
            this.clearFormButton.UseVisualStyleBackColor = true;
            this.clearFormButton.Click += new System.EventHandler(this.ClearFormButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 450);
            this.Controls.Add(this.clearFormButton);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.meterOutputLabel);
            this.Controls.Add(this.feetToMetersLabel);
            this.Controls.Add(this.feetInputTextBox);
            this.Controls.Add(this.feetLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Feet into Meters";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label feetLabel;
        private System.Windows.Forms.TextBox feetInputTextBox;
        private System.Windows.Forms.Label feetToMetersLabel;
        private System.Windows.Forms.Label meterOutputLabel;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.Button clearFormButton;
    }
}

